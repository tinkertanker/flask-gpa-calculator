# flask-gpa-calculator

A Flask GPA calculator demo. Made for teaching the 2020 GCE A-level H2 Computing syllabus.

## Setup instructions
* Install [Python 3](https://python.org/downloads/).
* Install Flask and some other dependencies. Necessary dependencies can be found by attempting to start the server.
* Run `python3 server.py` to start the server.
